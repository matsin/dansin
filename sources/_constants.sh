#!/usr/bin/env bash

DEFAULT_SERVICE=vazhsin
DEFAULT_DATABASE=vazhsin_db

# Set the path for the initial backup file
INITIAL_BACKUP="db_initial_backup.sql.gz"

DBMS_BACKUP_FILE_PREFIX='mariadb'
